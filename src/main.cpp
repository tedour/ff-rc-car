/*Copyright (C) 2018 - 2019  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */



#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <FS.h>
#include <WiFiClient.h>
#include <TimeLib.h>
#include <NtpClientLib.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ESP8266mDNS.h>
#include <Ticker.h>
#include <ArduinoOTA.h>
#include <ArduinoJson.h>
#include <FSWebServerLib.h>
#include <Hash.h>

#include <Servo.h>


#define CONFIG_CAR_FILE "/config-car.json"

#define PIN_LED_RED D4 // D1 Orange wire
#define PIN_LED_WHITE D2 // TX pin

Ticker t_blink;
void blink() {
    digitalWrite(PIN_LED_RED, !digitalRead(PIN_LED_RED));   // turn the LED on (HIGH is the voltage level)
    digitalWrite(PIN_LED_WHITE, !digitalRead(PIN_LED_WHITE));   // turn the LED on (HIGH is the voltage level)
}


// Servo configuration
Servo servo;
#define PIN_SERVO D1 // D1 Orange wire
int configServoLeft, configServoCenter, configServoRight;

// DC motor configuration
#define PIN_MOTOR_PWM D5 // D5 Motor  PWM Speed B-1A
#define PIN_MOTOR_DIR D6 // D6 Motor  Direction B-1B
#define PIN_MOTOR2_PWM D7 // D7 Motor2 PWM Speed A-1A
#define PIN_MOTOR2_DIR D3 // D3 Motor2 Direction A-1B
int motorSpeed, motorMinSpeed, motorMaxSpeed;
int motor2Speed, motor2MinSpeed, motor2MaxSpeed;
bool motorDirection, motor2Direction;
int motorInverse, motor2Inverse;
int motorDifferencial;


AsyncWebSocket ws("/ws");



/***
 * return the position of the space rank
 * String command = "M100 10";
 * byte result = indexOfRank(command, 1);
 * return 4
 * if not found return 0
 **/

byte commandIndexOfRank (String data, byte rank) {
  byte i = 0;
  byte index = 0;
  while (index != 255 && i < rank) {
  	index = data.indexOf(' ', index + 1);
    i++;
  }
  if (index == 255) {
    return 0;
  }
  return index;
}

/**
 * Return the parameter of a command
 * Sample :
 * String command = "M100 Z -10 A";
 * String result = getParameter(command, 1);
 * return Z
 * If no result, return false
 */
String commandGetParameter (String data, byte rank) {
  byte start;
  byte end;
  //Start
  if (rank == 0) {
    start = 0;
  } else {
    start = commandIndexOfRank(data, rank);
    if (start == 0) {
      //No paramater at this rank
      return "";
    }
    start ++; // remove space
  }

  //End
  end = commandIndexOfRank(data, rank+1);
  if (end == 0) {
    //End of string
    end = data.length();
  }
  String result = data.substring(start, end);
  result.trim();
  return result;
}

/* Move servo to position */
void setServo(int position) {
  servo.write(position);
}

/* Set servo position : SP 105 */
void commandSP(String inputString, AsyncWebSocketClient * client) {
  setServo(commandGetParameter(inputString, 1).toInt());
}

void updateDCMotor () {
  if (motorInverse == 1) {
    motorDirection = !motorDirection;
  }
  if (!motorDirection) {
    digitalWrite( PIN_MOTOR_DIR, LOW );
    analogWrite( PIN_MOTOR_PWM, motorSpeed );
  } else {
    digitalWrite( PIN_MOTOR_DIR, HIGH );
    analogWrite( PIN_MOTOR_PWM, 1024 - motorSpeed );
  }
}

void updateDCMotor2 () {
  if (motor2Inverse == 1) {
    motor2Direction = !motor2Direction;
  }
  if (!motor2Direction) {
    digitalWrite( PIN_MOTOR2_DIR, LOW );
    analogWrite( PIN_MOTOR2_PWM, motor2Speed );
  } else {
    digitalWrite( PIN_MOTOR2_DIR, HIGH );
    analogWrite( PIN_MOTOR2_PWM, 1024 - motor2Speed );
  }
}

void loadCarConfig() {
  // Store in userconfig.json
  // Load config if there is and create default if there is not

  ESPHTTPServer.load_user_config("configServoLeft", configServoLeft);
  if (configServoLeft == 0) {
    ESPHTTPServer.save_user_config("configServoLeft", 10);// TODO ? default is 10, not possible 0
  }
  ESPHTTPServer.load_user_config("configServoCenter", configServoCenter);
  if (configServoCenter == 0) {
    ESPHTTPServer.save_user_config("configServoCenter", 75);
  }
  ESPHTTPServer.load_user_config("configServoRight", configServoRight);
  if (configServoRight == 0) {
    ESPHTTPServer.save_user_config("configServoRight", 140);
  }

  ESPHTTPServer.load_user_config("motorInverse", motorInverse);
  if (motorInverse == 0) {
    ESPHTTPServer.save_user_config("motorInverse", 0);
  }
  ESPHTTPServer.load_user_config("motor2Inverse", motor2Inverse);
  if (motor2Inverse == 0) {
    ESPHTTPServer.save_user_config("motor2Inverse", 0);
  }
  ESPHTTPServer.load_user_config("speedDifferencial", motorDifferencial);
  if (motorDifferencial == 0) {
    ESPHTTPServer.save_user_config("speedDifferencial", 0);
  }
}


/* Set Motor speed : MS motor_speed motor_direction (0 or 1, 0 is default, 1 is reverse) */
void commandMS(String inputString, AsyncWebSocketClient * client) {
  motorDirection = commandGetParameter(inputString, 2).toInt();
  //client->text(commandGetParameter(inputString, 2));
  motorSpeed = commandGetParameter(inputString, 1).toInt();
  updateDCMotor();
}

/* Set Motor speed : MS2 motor_speed motor_direction (0 or 1, 0 is default, 1 is reverse) */
void commandMS2(String inputString, AsyncWebSocketClient * client) {
  motor2Direction = commandGetParameter(inputString, 2).toInt();
  //client->text(commandGetParameter(inputString, 2));
  motor2Speed = commandGetParameter(inputString, 1).toInt();
  updateDCMotor2();
}
long map2(long x, long in_min, long in_max, long out_min, long out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
 /* Calculate speed with two motors and diffrential : S speed direction */
 void commandS(String inputString, AsyncWebSocketClient * client) {
   int servoPosition = commandGetParameter(inputString, 1).toInt();  //servo
   setServo(servoPosition);
   motorDirection = commandGetParameter(inputString, 3).toInt();
   motor2Direction = commandGetParameter(inputString, 3).toInt();
   int mySpeed = commandGetParameter(inputString, 2).toInt();
   //configServoLeft, configServoCenter, configServoRight
   //motorDifferencial
   if (servoPosition == configServoCenter) {
     motorSpeed = mySpeed;
     motor2Speed = mySpeed;
   }
   if (servoPosition < configServoCenter) {
     // Calculate how it turn to left :
     // Left : turn = 0, Center : turn = 100
     int turn = map2(servoPosition, configServoLeft, configServoCenter, 0 , 100);
     // Add diffrential parameter
     turn = turn + (100 - turn) * motorDifferencial / 100;
     motorSpeed = mySpeed;
     motor2Speed = mySpeed * turn / 100;
   }
   if (servoPosition > configServoCenter) {
     // Right : turn = 0, Center : turn = 100
     // Calculate how it turn to Right :
     int turn = map2(servoPosition, configServoRight, configServoCenter, 0 , 100);
     // Add diffrential parameter
     turn = turn + (100 - turn) * motorDifferencial / 100;
     motorSpeed = mySpeed * turn / 100;
     motor2Speed = mySpeed;
   }
   /*String debug_message = "motor:";
   debug_message = debug_message + servoPosition;
   debug_message = debug_message + '|';
   debug_message = debug_message + mySpeed;
   debug_message = debug_message + '|';
   debug_message = debug_message + motorDifferencial;
   debug_message = debug_message + '|';
   debug_message = debug_message + motorSpeed;
   debug_message = debug_message + ',';
   debug_message = debug_message + motor2Speed;
   client->text(debug_message);*/
   updateDCMotor();
   updateDCMotor2();
 }

// set Motors values M servo_position speed direction
void commandM(String inputString, AsyncWebSocketClient * client) {
  //servo
  setServo(commandGetParameter(inputString, 1).toInt());
  //Speed
  motorDirection = commandGetParameter(inputString, 3).toInt();
  //client->text(commandGetParameter(inputString, 2));
  motorSpeed = commandGetParameter(inputString, 2).toInt();
  updateDCMotor();
}

/* Set Motor inverse : MI motor_inverse (0 or 1) */
void commandMI(String inputString, AsyncWebSocketClient * client) {
  motorInverse = commandGetParameter(inputString, 1).toInt();
}

/* Set Motor2 inverse : MI2 motor_inverse (0 or 1) */
void commandMI2(String inputString, AsyncWebSocketClient * client) {
  motor2Inverse = commandGetParameter(inputString, 1).toInt();
}

/* Set Motor differencial */
void commandMD(String inputString, AsyncWebSocketClient * client) {
  motorDifferencial = commandGetParameter(inputString, 1).toInt();
}

/* Save to JSON : SJ key value */
void commandSJ(String inputString, AsyncWebSocketClient * client) {
  // Save value to JSON
  // Example : SJconfigServoMax|180
  ESPHTTPServer.save_user_config(commandGetParameter(inputString, 1), commandGetParameter(inputString, 2));
}

// Load car config : LC (no parameters)
void commandLC(String inputString, AsyncWebSocketClient * client) {
  loadCarConfig();
  client->text("LC");
}

// Start in AP mode or Client Wifi mode : AP ap, AP client, AP info
void commandAP(String inputString, AsyncWebSocketClient * client) {
  if (commandGetParameter(inputString, 1) == "ap") {
    if (SPIFFS.exists("/config.json")) {
      /*FSInfo fsInfo;
      SPIFFS.info(fsInfo);
      String debug_message = "fsInfo:";
      debug_message = debug_message + fsInfo.usedBytes;
      debug_message = debug_message + '/';
      debug_message = debug_message + fsInfo.totalBytes;
      client->text(debug_message);*/
      if (SPIFFS.exists("/config1.json")) SPIFFS.remove("/config1.json");
      bool res = SPIFFS.rename("/config.json", "/config1.json");
      if (res) client->text("Next restart in AP mode");
    }
  }

  if (commandGetParameter(inputString, 1) == "client") {
    if (SPIFFS.exists("/config1.json")) {
      if (SPIFFS.exists("/config.json")) SPIFFS.remove("/config.json");
      bool res = SPIFFS.rename("/config1.json", "/config.json");
      if (res) client->text("Next restart in Client mode");
    }
  }
}

// Command for Arduino Serial : A B 0 : Send command B 0 to Arduino
void commandA(String inputString, AsyncWebSocketClient * client) {
  Serial.println(inputString);
}

void dispatchCommand(String inputString, AsyncWebSocketClient * client) {
  //client->text(inputString);
  String mainCommand = commandGetParameter(inputString, 0);
  if (mainCommand == "M") commandM(inputString, client); // set Motors values M servo_position speed direction
  if (mainCommand == "SP") commandSP(inputString, client); // Set servo position : SP 75
  if (mainCommand == "MS") commandMS(inputString, client); // Set Motor speed : MS motor_speed motor_direction (0 or 1, 0 is default)
  if (mainCommand == "MS2") commandMS2(inputString, client); // Set 2nd Motor speed : MS2 motor_speed motor_direction (0 or 1, 0 is default)
  if (mainCommand == "S") commandS(inputString, client); // Calculate speed with two motors and diffrential : S servo_position speed direction
  if (mainCommand == "MI") commandMI(inputString, client); // Set Motor inverse : MI motor_inverse (0 or 1)
  if (mainCommand == "MI2") commandMI2(inputString, client); // Set Motor inverse : MI motor_inverse (0 or 1)
  if (mainCommand == "MD") commandMD(inputString, client); // Set motor Diffrencial : MD value
  if (mainCommand == "SJ") commandSJ(inputString, client); // Save to JSON : SJ key value
  if (mainCommand == "LC") commandLC(inputString, client); // Load car config : LC (no parameters)
  if (mainCommand == "AP") commandAP(inputString, client); // Start in AP mode or Client Wifi mode : AP ap, AP client, AP info
  if (mainCommand == "A") commandA(inputString, client); // Command for Arduino Serial : A B 0 : Send command B 0 to Arduino
}

void onWsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
  if(type == WS_EVT_CONNECT){
    //client connected
    os_printf("ws[%s][%u] connect\n", server->url(), client->id());
    client->printf("Hello Client %u :)", client->id());
    client->ping();
  } else if(type == WS_EVT_DISCONNECT){
    //client disconnected
    os_printf("ws[%s][%u] disconnect: %u\n", server->url(), client->id());
  } else if(type == WS_EVT_ERROR){
    //error was received from the other end
    os_printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
  } else if(type == WS_EVT_PONG){
    //pong message was received (in response to a ping request maybe)
    os_printf("ws[%s][%u] pong[%u]: %s\n", server->url(), client->id(), len, (len)?(char*)data:"");
  } else if(type == WS_EVT_DATA){
    //data packet
    AwsFrameInfo * info = (AwsFrameInfo*)arg;
    if(info->final && info->index == 0 && info->len == len){
      //the whole message is in a single frame and we got all of it's data
      os_printf("ws[%s][%u] %s-message[%llu]: ", server->url(), client->id(), (info->opcode == WS_TEXT)?"text":"binary", info->len);
      if(info->opcode == WS_TEXT){
        data[len] = 0;
        os_printf("%s\n", (char*)data);
      } else {
        for(size_t i=0; i < info->len; i++){
          os_printf("%02x ", data[i]);
        }
        os_printf("\n");
      }
      if(info->opcode == WS_TEXT) {
        String inputString;
        inputString = (char*)data;
        dispatchCommand(inputString, client);

        if (data[0] == 'I') {
          // Transform to string
          String inputString;
          inputString = (char*)data;
          float myvalue;
          myvalue = inputString.substring(1,4).toFloat() / 100;
          t_blink.attach(myvalue, blink);
        }
      } else {
        client->binary("I got your binary message");
      }
    } else {
      //message is comprised of multiple frames or the frame is split into multiple packets
      if(info->index == 0){
        if(info->num == 0)
          os_printf("ws[%s][%u] %s-message start\n", server->url(), client->id(), (info->message_opcode == WS_TEXT)?"text":"binary");
        os_printf("ws[%s][%u] frame[%u] start[%llu]\n", server->url(), client->id(), info->num, info->len);
      }

      os_printf("ws[%s][%u] frame[%u] %s[%llu - %llu]: ", server->url(), client->id(), info->num, (info->message_opcode == WS_TEXT)?"text":"binary", info->index, info->index + len);
      if(info->message_opcode == WS_TEXT){
        data[len] = 0;
        os_printf("%s\n", (char*)data);
      } else {
        for(size_t i=0; i < len; i++){
          os_printf("%02x ", data[i]);
        }
        os_printf("\n");
      }

      if((info->index + len) == info->len){
        os_printf("ws[%s][%u] frame[%u] end[%llu]\n", server->url(), client->id(), info->num, info->len);
        if(info->final){
          os_printf("ws[%s][%u] %s-message end\n", server->url(), client->id(), (info->message_opcode == WS_TEXT)?"text":"binary");
          if(info->message_opcode == WS_TEXT)
            client->text("I got your text message2");
          else
            client->binary("I got your binary message");
        }
      }
    }
  }
}



void saveCarConfig() {

}



void setup() {
   Serial.end();
    // DC MOTOR
    pinMode( PIN_MOTOR_DIR, OUTPUT );
    pinMode( PIN_MOTOR_PWM, OUTPUT );
    digitalWrite( PIN_MOTOR_DIR, LOW );
    digitalWrite( PIN_MOTOR_PWM, LOW );

    // DC MOTOR 2
    // I must define it even it is not used
    pinMode( PIN_MOTOR2_DIR, OUTPUT );
    pinMode( PIN_MOTOR2_PWM, OUTPUT );
    digitalWrite( PIN_MOTOR2_DIR, LOW );
    digitalWrite( PIN_MOTOR2_PWM, LOW );

    // WiFi is started inside library
    SPIFFS.begin(); // Not really needed, checked inside library and started if needed
    ESPHTTPServer.begin(&SPIFFS);
    /* add setup code here */
    ESPHTTPServer.on("/heap", HTTP_GET, [](AsyncWebServerRequest *request){
      int args = request->args();
      for(int i=0;i<args;i++){
        Serial.printf("ARG[%s]: %s\n", request->argName(i).c_str(), request->arg(i).c_str());
      }
      request->send(200, "text/plain", "hello !");
    });
    ESPHTTPServer.on("/command", HTTP_GET, [](AsyncWebServerRequest *request){
      // Send a command via http : http://192.168.1.140/command?command=SP%20100
      int args = request->args();
      String inputString;
      for(int i=0;i<args;i++){

        Serial.printf("ARG[%s]: %s\n", request->argName(i).c_str(), request->arg(i).c_str());
        //if (request->argName(i).c_str() == "command") {
          inputString = request->arg(i).c_str();
        //}
      }
      Serial.println(inputString);
      dispatchCommand(inputString, NULL);
      request->send(200, "text/plain", "command sended !");
    });

    // t_blink
    pinMode(2, OUTPUT);
    t_blink.attach(0.5, blink);

    ws.onEvent(onWsEvent);
    ESPHTTPServer.addHandler(&ws);

    loadCarConfig();
    servo.write(configServoCenter); // TODO : Get the configServoCenter value
    servo.attach(PIN_SERVO);  //D1



}

void loop() {
    /* add main program code here */

    // DO NOT REMOVE. Attend OTA update from Arduino IDE
    ESPHTTPServer.handle();

}
