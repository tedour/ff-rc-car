/*Copyright (C) 2018 - 2019  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */


/** JSON Values **/

var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    // Get values from JSON
    var myJSON = JSON.parse(this.responseText);
    console.log(myJSON);
    setTimeout(directionLoadValues(myJSON), 500); // TODO : Error ?
    setTimeout(speedLoadValues(myJSON), 1000);
    setTimeout(orientationLoadValues(myJSON), 700);

  }
};
xmlhttp.open("GET", "userconfig.json", true);
xmlhttp.send();
/** End JSON Values **/

/** Websocket **/
var connection = new WebSocket('ws://' + location.hostname + ':80/ws', ['arduino']);
connection.onopen = function () {
  connection.send('Connect ' + new Date());
};
connection.onerror = function (error) {
  console.log('WebSocket Error ', error);
};
connection.onmessage = function (e) {
  console.log('Server: ', e.data);
};
connection.onclose = function () {
  console.log('WebSocket connection closed');
};
/** End Websocket **/

/** Phone orientation control **/

if(window.DeviceOrientationEvent){
  window.addEventListener("deviceorientation", orientation, false);
}else{
  console.log("DeviceOrientationEvent is not supported");
}

var orientationSpeedValuePrevious = 0;
var orientationDirectionValuePrevious = 0;
var orientationStartPlay = 0;
var orientationMinForward, orientationMaxForward, orientationMinRear, orientationMaxRear, orientationLeftRight;
function orientationPlay() {
  if (document.getElementById('orientation-start-play').checked) {
    orientationStartPlay = 1; // start
  } else {
    orientationStartPlay = 0;
    // Stop car
    speedValue = 0;
    speed2Value = 0;
    speedUpdateValue();
    speed2UpdateValue();
  }
}

var orientationSpeedValuePrevious, orientationDirectionValuePrevious;
function orientation(event){
  // Refresh html
  var orientation = document.getElementById('orientationAlpha');
  orientation.value = parseInt(event.alpha);
  var orientation = document.getElementById('orientationBeta');
  orientation.value = parseInt(event.beta);
  var orientation = document.getElementById('orientationGamma');
  orientation.value = parseInt(event.gamma);
  if (orientationStartPlay) {
    if (event.gamma < 0) {
      //Speed Forward
      // Direction
      orientationDirectionValue = utilsMapValue(parseInt(event.beta), parseInt(-orientationLeftRight), parseInt(orientationLeftRight), parseInt(directionLeft), parseInt(directionRight));
      //Speed
      orientationSpeedValue = utilsMapValue(parseInt(event.gamma), parseInt(orientationMinForward), parseInt(orientationMaxForward), 0, 1024);
      orientationSpeedDirection = 0;
    } else {
      //Speed Rear
      //Direction
      if (event.beta > 0) {
        orientationDirectionValue = utilsMapValue(180 - parseInt(event.beta), parseInt(-orientationLeftRight), parseInt(orientationLeftRight), parseInt(directionLeft), parseInt(directionRight));
      } else {
        orientationDirectionValue = utilsMapValue(-(180 + parseInt(event.beta)), parseInt(-orientationLeftRight), parseInt(orientationLeftRight), parseInt(directionLeft), parseInt(directionRight));
      }
      // Speed
      orientationSpeedValue = utilsMapValue(parseInt(event.gamma), parseInt(orientationMinRear), parseInt(orientationMaxRear), 0, 1024);
      orientationSpeedDirection = 1;
    }
    orientationDirectionValue = parseInt(orientationDirectionValue);
    orientationSpeedValue = parseInt(orientationSpeedValue);
    document.getElementById("orientation-speed-debug").value = orientationSpeedValue;
    document.getElementById("orientation-direction-debug").value = orientationDirectionValue;

    if (orientationDirectionValuePrevious != orientationDirectionValue || orientationSpeedValuePrevious != orientationSpeedValue) {
      orientationDirectionValuePrevious = orientationDirectionValue;
      orientationSpeedValuePrevious = orientationSpeedValue;
      if (!speed2Activate2ndMotor) {
        connection.send('M ' + orientationDirectionValue + ' ' + orientationSpeedValue + ' ' + orientationSpeedDirection);
      } else {
        connection.send('S ' + orientationDirectionValue + ' ' + orientationSpeedValue + ' ' + orientationSpeedDirection);
      }

    }

  }
}

function orientationLoadValues(myJSON) {
  orientationMinForward = myJSON.orientationMinForward;
  orientationMaxForward = myJSON.orientationMaxForward;
  orientationMinRear = myJSON.orientationMinRear;
  orientationMaxRear = myJSON.orientationMaxRear;
  orientationLeftRight = myJSON.orientationLeftRight;
  document.getElementById("orientation-min-forward").value = myJSON.orientationMinForward;
  document.getElementById("orientation-max-forward").value = myJSON.orientationMaxForward;
  document.getElementById("orientation-min-rear").value = myJSON.orientationMinRear;
  document.getElementById("orientation-max-rear").value = myJSON.orientationMaxRear;
  document.getElementById("orientation-left-right").value = myJSON.orientationLeftRight;
  document.getElementById('orientation-left-right-negative').value = 180 - Number(myJSON.orientationLeftRight);
}

function orientationSaveValues() {
  // save data to userconfig.json
  utilsSaveToJSON('orientationMinForward', document.getElementById("orientation-min-forward").value);
  utilsSaveToJSON('orientationMaxForward', document.getElementById("orientation-max-forward").value);
  utilsSaveToJSON('orientationMinRear', document.getElementById("orientation-min-rear").value);
  utilsSaveToJSON('orientationMaxRear', document.getElementById("orientation-max-rear").value);
  utilsSaveToJSON('orientationLeftRight', document.getElementById("orientation-left-right").value);

}

/** End Phone orientation control **/

/** Start Direction **/
var directionPosition;
var directionLeft, directionCenter, directionRight;

function directionUpdateValue() {
  var directionSlider = document.getElementById('direction-slider');
  directionSlider.value = directionPosition;
  var directionText = document.getElementById('direction-text');
  directionText.value = directionPosition;
  connection.send('SP ' + directionPosition);
}

function directionLoadValues(myJSON) {
  directionLeft = myJSON.configServoLeft;
  directionCenter = myJSON.configServoCenter;
  directionRight = myJSON.configServoRight;
  directionPosition = directionCenter;
  document.getElementById("direction-slider").min = directionLeft;
  document.getElementById("direction-slider").value = directionCenter;
  document.getElementById("direction-slider").max = directionRight;
  document.getElementById('direction-text').value = directionPosition;
}

function directionSaveValues() {
  // save data to userconfig.json
  utilsSaveToJSON('configServoLeft', directionLeft);
  utilsSaveToJSON('configServoCenter', directionCenter);
  utilsSaveToJSON('configServoRight', directionRight);
  connection.send('LC');
}
/** End direction **/

/** Speed **/
var speedValue = 0;
var speedDirection = 0;
var speedInverse = false;
var speedActivate2ndMotor = 0;
var speedDifferencial = 100;

// 2nd motor
var speed2Value = 0;
var speed2Direction = 0;
var speed2Inverse = false;
var speed2Activate2ndMotor = false;

function speedUpdateValue() {
  var speedSlider = document.getElementById('speed-slider');
  speedSlider.value = speedValue;
  var speedText = document.getElementById('speed-text');
  speedText.value = speedValue;
  connection.send('MS ' + speedValue + ' ' + speedDirection);
}

function speedDirectionUpdate() {
  if (document.getElementById('speed-direction').checked) {
    speedDirection = 1; // Rear
  } else {
    speedDirection = 0;
  }
  speedUpdateValue();
}

function speedDirectionInverse() {
  if (document.getElementById('speed-inverse').checked) {
    speedInverse = 1; // Rear
  } else {
    speedInverse = 0;
  }
  speedSaveValues();
  connection.send('MI ' + speedInverse);
}

function speedLoadValues(myJSON) {
  speedInverse = myJSON.motorInverse;
  console.log("speedInverse" + speedInverse);
  if (speedInverse == 1) {
    document.getElementById("speed-inverse").checked = true;
    console.log("speedtrue:" + speedInverse);
  } else {
    document.getElementById("speed-inverse").checked = false;
    console.log("speedfalse:" + speedInverse);
  }
  // 2nd motor
  speed2Activate2ndMotor = myJSON.motor2nd;
  if (speed2Activate2ndMotor == 1) {
    document.getElementById("speed-activate-2nd").checked = true;
  } else {
    document.getElementById("speed-activate-2nd").checked = false;
  }
  speedActivate2nd(false);
  // Differencial
  speedDifferencial = myJSON.speedDifferencial;
  document.getElementById("speed-slider-differential").value = speedDifferencial;
  document.getElementById("speed-text-differential").value = speedDifferencial;
}

function speedSaveValues() {
  // save data to userconfig.json
  utilsSaveToJSON('motorInverse', speedInverse);
  utilsSaveToJSON('motor2Inverse', speed2Inverse);
  utilsSaveToJSON('motor2nd', speedActivate2ndMotor);
  utilsSaveToJSON('speedDifferencial', speedDifferencial);
  connection.send('LC');
}


function speedActivate2nd(update) {
  if (document.getElementById('speed-activate-2nd').checked) {
    speedActivate2ndMotor = 1; // Rear
    var elems = document.getElementsByClassName("speed-2nd");
    for(var i = 0; i != elems.length; ++i) {
      elems[i].style.display = "block";
    }

  } else {
    speedActivate2ndMotor = 0;
    var elems = document.getElementsByClassName("speed-2nd");
    for(var i = 0; i != elems.length; ++i) {
      elems[i].style.display = "none";
    }
  }
  if (update) {
    speedUpdateValue();
  }
}


function speed2UpdateValue() {
  var speedSlider = document.getElementById('speed-slider-2nd');
  speedSlider.value = speed2Value;
  var speedText = document.getElementById('speed-text-2nd');
  speedText.value = speed2Value;
  connection.send('MS2 ' + speed2Value + ' ' + speed2Direction);
}

function speed2DirectionUpdate() {
  if (document.getElementById('speed-direction-2nd').checked) {
    speed2Direction = 1; // Rear
  } else {
    speed2Direction = 0;
  }
  speed2UpdateValue();
}

function speed2DirectionInverse() {
  if (document.getElementById('speed-inverse-2nd').checked) {
    speed2Inverse = 1; // Rear
  } else {
    speed2Inverse = 0;
  }
  connection.send('MI2 ' + speedInverse);
}

function speedDifferentialUpdate() {
  var speedSlider = document.getElementById('speed-slider-differential');
  speedSlider.value = speedDifferencial;
  var speedText = document.getElementById('speed-text-differential');
  speedText.value = speedDifferencial;
  //connection.send('MD ' + speedDifferential);

}


/** End Speed **/


function utilsSaveToJSON(key, value) {
  connection.send('SJ ' + key + ' ' + value);
}

function utilsMapValue(x, in_min, in_max, out_min, out_max)
{
  if (in_min < in_max) {
    if (x < in_min) x = in_min;
    if (x > in_max) x = in_max;
  } else {
    if (x > in_min) x = in_min;
    if (x < in_max) x = in_max;
  }
  return ((x - in_min) * (out_max - out_min) / (in_max - in_min)) + out_min;
}
